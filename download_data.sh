export FOLDER_NAME=${PWD##*/}
echo $FOLDER_NAME
export DATA_DIR=../../data/$FOLDER_NAME
echo $DATA_DIR
mkdir -p $DATA_DIR
cd $DATA_DIR
export PROJECT_PATH="http://snap.stanford.edu/data/amazon/productGraph/categoryFiles/"
export META_PATH=$PROJECT_PATH'meta_'$FOLDER_NAME'.json.gz'
export REVIEWS_PATH=$PROJECT_PATH'reviews_'$FOLDER_NAME'.json.gz'
echo $META_PATH
echo $REVIEWS_PATH
wget $META_PATH
wget $REVIEWS_PATH

export META_JSON=$DATA_DIR/'meta_'$FOLDER_NAME'.json.gz'
export REVIEWS_JSON=$DATA_DIR/'reviews_'$FOLDER_NAME'.json.gz'

gunzip $META_JSON
gunzip $REVIEWS_JSON