#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Shubham Agarwal, Nov 2017
# This script reads reviews_*.json 
# Dataset: Amazon Product data 
# To run this script provide json_file and output_file_path
# Run as:
# python filter_reviews_pyspark.py 'path/to/input_json' 'path/to/output'
import os 
import sys
import argparse
import json
from pyspark.sql import HiveContext
from pyspark import SparkContext
from pyspark.sql import functions as f
from pyspark.sql.types import *
# checkString to filter reviews
checkString = ['compared','nevertheless','contrast','contrary','than ','unlike', 'better ']
# Schema of output function
schema = StructType([
    StructField("numLines", IntegerType(), False),
    StructField("reviewFeature", IntegerType(), False)
])

def return_length_feature_string(targetLine):
    # Check if any substring from list (checkString) is found in targetLine
    num_lines = len(targetLine.split('. '))
    if any(substr in targetLine.lower() for substr in checkString):    
        return (num_lines,1)
    else:
        return (num_lines,0)

def decode(args):
	# Spark context and read json file
	sc = SparkContext()
	hive_contxt = HiveContext(sc)
	json_data = hive_contxt.read.json(args.json_file)
	print(json_data.take(2))
	# Loading data as DF. Use UDFs
	user_def_func = f.UserDefinedFunction(return_length_feature_string, schema)
	json_data_features = json_data.withColumn('reviewFeature', user_def_func(json_data['reviewText']))
	json_data_features = json_data_features.where(json_data_features['reviewFeature.reviewFeature'] == 1)
	json_data_features = json_data_features.sort("reviewFeature.numLines",ascending=True)
	json_data_features.count()
	# Save as json
	json_data_features.write.save(args.output_file_path,format="json")	
	# json_data_features.write.format('json').save(args.output_file_path) # didnt work, writing in folder 'file_name.json' and files with part-XXX
	# json_data_features.toJSON().saveAsTextFile(args.output_file_path)  # didnt work, writing in folder 'file_name.json' and files with part-XXX

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('json_file', help='Input JSON file')
	# parser.add_argument("--dataDirectory",dest="dataDirectory",type=str,help="Data directory path") Optional argument
    parser.add_argument('output_file_path', help='Output file path')
    args = parser.parse_args()
    decode(args)
