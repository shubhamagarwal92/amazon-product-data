export FOLDER_NAME=${PWD##*/}
echo $FOLDER_NAME
export DATA_DIR=../../data/$FOLDER_NAME/
echo $DATA_DIR
export INPUT_JSON=$DATA_DIR/'reviews_'$FOLDER_NAME'.json'
export OUTPUT_DIR=$DATA_DIR/'filtered_reviews_'$FOLDER_NAME'.json'

python filter_reviews_pyspark.py $INPUT_JSON $OUTPUT_DIR